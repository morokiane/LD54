extends Node
var soundPath = "res://SFX/"

@onready var soundPlayers = get_children()

var sounds = {
	"coin" : load(soundPath + "Coin.wav"),
	"step" : load(soundPath + "Step.wav"),
	"door" : load(soundPath + "Door.wav"),
	"key" : load(soundPath + "Key.wav"),
	"oil" : load(soundPath + "Oil.wav"),
	"chest" : load(soundPath + "Chest.wav"),
	"moneyBag" : load(soundPath + "MoneyBag.wav"),
	"fireBall2" : load(soundPath + "FireBall2.wav"),
	"fireBall" : load(soundPath + "FireBall.wav"),
	"impDie" : load(soundPath + "ImpDie.wav"),
	"playerHit" : load(soundPath + "PlayerHit.wav"),
	"lantern" : load(soundPath + "Lantern.wav"),
	"monsterGrowl" : load(soundPath + "MonsterGrowl.wav")
}

func play(soundString):
	for soundPlayer in soundPlayers:
		if not soundPlayer.playing:
			soundPlayer.stream = sounds[soundString]
			soundPlayer.play()
			return
