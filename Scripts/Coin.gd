extends Node3D

func _on_area_3d_body_entered(body):
	if body is Player:
		CollectCoin()

func CollectCoin():
	print("coin")
	if GameController.weight < 50:
		GameController.coinsCollect += 1
		GameController.weight += 2
		SoundFx.play("coin")
		queue_free()
