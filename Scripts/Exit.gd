extends Area3D
class_name Exit

@export_file("*.scn") var levelPath: String

func GetInteractionText():
	return "Interact"
	
func Interact():
	get_tree().change_scene_to_file(levelPath)
