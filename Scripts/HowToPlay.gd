extends Control

func _ready():
	$MarginContainer/TextureRect/BackBTN.grab_focus()

func _on_back_btn_pressed():
	get_tree().change_scene_to_file("res://Scenes/Title.scn")
