extends CharacterBody3D

@onready var sprite: Sprite3D = $Sprite3D
@onready var collision: CollisionShape3D = $CollisionShape3D
@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var playerDetect: CollisionShape3D = $PlayerDetect/CollisionShape3D
@onready var nav: NavigationAgent3D = $NavigationAgent3D
@onready var sfx: AudioStreamPlayer3D = $SFX

@export var moveSpeed: float = 4
@export var attackRange: float = 2
@export var fireball: PackedScene

@export var animCol: int = 0

var dead: bool = false
var foundPlayer: bool = false
var attackPlayer: bool = false
var player: CharacterBody3D
var fireballExists: bool = false

func _ready():
	player = GameController.player
	anim.play("idle")

func _process(_delta):
	if dead:
		return
	
	if GameController.player.lanternOn && !foundPlayer:
		foundPlayer = true
	elif !GameController.player.lanternOn && foundPlayer:
		foundPlayer = false
		anim.play("idle")
		
	if attackPlayer && !fireballExists:
		anim.play("attack")
		var instance = fireball.instantiate()
		instance.position = Vector3(0, 1.2, 0)
		add_child(instance)
		SoundFx.play("fireBall2")
		fireballExists = true
		await get_tree().create_timer(3).timeout
		fireballExists = false

func _physics_process(_delta):
	if dead:
		return
	if player == null || !foundPlayer:
		return

	nav.set_target_position(player.global_transform.origin)
	var nextPoint = nav.get_next_path_position()
	velocity = (nextPoint - global_transform.origin).normalized() * moveSpeed
#	var direction = player.global_position - global_position
#	direction.y = 0
#	direction = direction.normalized()
#
#	velocity = direction * moveSpeed

	if velocity != Vector3(0,0,0):
		anim.play("walk")
		sfx.play()
	if !dead:
		move_and_slide()

#func _process(_delta):
#	if !foundPlayer:
#		var pFWD: Vector3 = -player.global_transform.basis.z
#		var fwd: Vector3 = global_transform.basis.z
#		var left: Vector3 = global_transform.basis.z
#
#		var lDot: float = left.dot(pFWD)
#		var fDot: float = fwd.dot(pFWD)
#		var row: int = 0
#
#		sprite.flip_h = false
#
#		if fDot < -0.85:
#			row = 0
#		elif fDot > 0.85:
#			row = 4
#		else:
#			sprite.flip_h = lDot > 0
#			if abs(fDot) < 0.3:
#				row = 2
#			elif fDot < 0:
#				row = 1
#			else:
#				row = 3
#
#		sprite.frame = animCol + row * 4

func Kill():
	anim.play("death")
	SoundFx.play("impDie")
	dead = true
	collision.disabled = true
	playerDetect.disabled = true

func _on_player_detect_body_entered(body):
	if body is Player:
		attackPlayer = true

func _on_player_detect_body_exited(body):
	if body is Player:
		attackPlayer = false
		anim.play("idle")
