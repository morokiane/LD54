extends Node
class_name Interactable

#enum keyType {none, RedKey, yellow, blue}
@export_enum("None", "Lantern", "Gate", "YellowKey") var keyType: String
@export_enum("None", "Lantern", "Oil", "Chest", "Bag", "Bolts", "Crossbow") var objType: String

#@export_enum("Lever", "RedLight", "GreenLight") var switchType: String

var used: bool = false
var canUse: bool = false
var canPickup: bool = true

func GetInteractionText():
	return "Interact"
	
func Interact():
	print("Interacted with %s" % name)
	
	if keyType == "Lantern":
		GameController.keys[keyType] = true
		GameController.weight += 1
		SoundFx.play("key")
		queue_free()
		
	if keyType == "Gate":
		GameController.keys[keyType] = true
		GameController.weight += 1
		SoundFx.play("key")
		queue_free()
		
	if objType == "Lantern":
		GameController.hasLantern = true
		GameController.weight += 5
		SoundFx.play("lantern")
		queue_free()
		
	if objType == "Oil" && GameController.oilLevel < 1:
		GameController.oilLevel += .25
		SoundFx.play("oil")
		queue_free()
		
	if objType == "Bag":
		GameController.coinsCollect += 10
		GameController.weight += 20
		SoundFx.play("moneyBag")
		queue_free()
		
	if objType == "Bolts":
		GameController.boltsAmmo =+ 5
		GameController.weight += 5
		SoundFx.play("lantern")
		queue_free()
		
	if objType == "Crossbow":
		GameController.boltsAmmo =+ 1
		GameController.hasCrossbow = true
		GameController.weight += 10
		SoundFx.play("lantern")
		queue_free()
	
	if objType == "Chest":
		$AnimationPlayer.play("open")
		GameController.totalCoinsCollect = GameController.coinsCollect
		GameController.weight -= GameController.coinsCollect * 2
		GameController.coinsCollect = 0
		SoundFx.play("chest")

		await get_tree().create_timer(10).timeout
		
		$AnimationPlayer.play("close")

#	if GameController.keys.has(keyType):
#		canUse = true
#
#		used = true
#	else:
#		return

