extends Control

@onready var hSlider = $HSlider
@onready var invertMouse = $InvertMouse
@onready var fullScreen = $FullScreen

func _ready():
#	hSlider.value = GameController.mouseSpeed
	print(GameController.mouseSpeed)
	if !GameController.invertLook:
		invertMouse.button_pressed = false
	else:
		invertMouse.button_pressed = true
	
	fullScreen.toggle_mode = true
	fullScreen.button_pressed = true

func _on_check_box_toggled(button_pressed):
	InvertMouse()

func InvertMouse():
	if !invertMouse.button_pressed:
		GameController.invertLook = false
		print("invert off")
	else:
		GameController.invertLook = true
		print("invert on")

func _on_back_btn_pressed():
	get_tree().change_scene_to_file("res://Scenes/Title.scn")

func _on_h_slider_drag_ended(value):
	GameController.mouseSpeed = hSlider.value

func _on_full_screen_toggled(button_pressed):
	FullScreen()
	
func FullScreen():
	if fullScreen.button_pressed:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
