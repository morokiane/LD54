extends CharacterBody3D

@export var moveSpeed: float = 4

var player: CharacterBody3D

func _ready():
	player = GameController.player

func _physics_process(_delta):

	var direction = player.global_position - global_position
	direction.y = -0.5
	direction = direction.normalized()

	velocity = direction * moveSpeed
	move_and_slide()

func _on_area_3d_body_entered(body):
	if body is Player:
		GameController.player.Kill()
		queue_free()

func _on_area_3d_area_entered(area):
	if area.is_in_group("environment"):
		SoundFx.play("fireBall")
		queue_free()

func _on_timer_timeout():
	SoundFx.play("fireBall")
	queue_free()
