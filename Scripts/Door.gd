extends Node
class_name Door

@onready var timer: Timer = $Timer
@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var interaction: CollisionShape3D = $CollisionShape3D
@onready var collision: CollisionShape3D = $Door/StaticBody3D/CollisionShape3D

@export var requireSwitch: bool = false
#@export var requireKey: bool = false
@export_enum("None", "Lantern", "Gate", "YellowKey") var keyType: String
@export var closes: bool = false

var canUse: bool = false

func _ready():
#	print(name,": ", "X:", self.position.x," ", "Y:", self.position.y)
	print(GameController.keys)

func GetInteractionText():
	if requireSwitch || keyType == "None":
		return "Interact"
	
func Interact():
	print("Interacted with %s" % name)
	if keyType == "None":
		canUse = true
	
	if GameController.keys.has(keyType) && keyType == "Lantern":
		canUse = true
		GameController.keys.erase("Lantern")
		GameController.weight -= 1
	
	if GameController.keys.has(keyType) && keyType == "Gate":
		canUse = true
		GameController.keys.erase("Gate")
		GameController.weight -= 1

	if canUse:
		DoorOpen()
	else:
		return

func DoorOpen():
	anim.play("open")
	SoundFx.play("door")
	interaction.disabled = true
	await get_tree().create_timer(2).timeout
	collision.disabled = true
	print(collision.disabled)
	
	if closes:
		timer.start()

func _on_timer_timeout():
	anim.play("close")
	timer.stop()
	await get_tree().create_timer(2).timeout
	collision.disabled = false
	interaction.disabled = false
