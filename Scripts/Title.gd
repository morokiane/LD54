extends Control

@onready var startBTN = $VBoxContainer/StartBTN

func _ready():
	get_tree().paused = false
	startBTN.grab_focus()
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func _on_start_btn_pressed():
	GameController.LoadScene(self, "town")
	get_tree().change_scene_to_file("res://Levels/Town.scn")

func _on_settings_btn_pressed():
	get_tree().change_scene_to_file("res://Scenes/Settings.scn")

func _on_exit_btn_pressed():
	get_tree().quit()

func _on_how_btn_pressed():
	get_tree().change_scene_to_file("res://Scenes/HowToPlay.scn")

func _input(_event):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
