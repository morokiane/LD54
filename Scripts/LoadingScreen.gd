extends CanvasLayer

signal safeToLoad

@onready var anim: AnimationPlayer = $AnimationPlayer

func FadeOut():
	anim.play("fadeout")
	await anim.animation_finished
	queue_free()
