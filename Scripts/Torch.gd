extends Node3D

@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var sprite: AnimatedSprite3D = $AnimatedSprite3D

var time: float = 0
var flip: float = 0

func _ready():
	time = randf_range(0, 0.5)
	flip = randi_range(0,1)
	anim.play("torch",0, time)
	
	if flip == 0:
		sprite.flip_h = false
	else:
		sprite.flip_h = true
