extends RayCast3D

@onready var interactUI: Label = $"../../../HUD/MarginContainer/Interaction"

var currentCollider

func _process(_delta):
	var collider = get_collider()
	
	if is_colliding() && collider is Interactable || collider is Door || collider is Exit:
		if currentCollider != collider:
			currentCollider = collider
			interactUI.show()
			
		if Input.is_action_just_pressed("interact"):
			collider.Interact()
	elif currentCollider:
		currentCollider = null
		interactUI.hide()
