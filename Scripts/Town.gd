extends Node3D
class_name Town

@onready var WE: WorldEnvironment = $WorldEnvironment

func _ready():
	WE.environment.background_energy_multiplier = 0.0
	
	if GameController.inDungeon:
		GameController.player.transform.origin = Vector3(-5.712, 0, -69.872)
		GameController.player.rotate_y(-171)
		GameController.inDungeon = false
#	new_node.transform.origin = $Position3D.transform.origin
	print(GameController.mouseSpeed)
