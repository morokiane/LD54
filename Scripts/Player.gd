extends CharacterBody3D
class_name Player

@onready var gunRay: RayCast3D = $Head/Camera3D/GunRay
@onready var interact: RayCast3D = $Head/Camera3D/Interact
@onready var cam: Camera3D = $Head/Camera3D
@onready var lantern: Node3D = $Lantern
@onready var oilLevel: TextureProgressBar = $HUD/HBoxContainer/TextureProgressBar
@onready var weightLevel: TextureProgressBar = $HUD/HBoxContainer/TextureProgressBar2
@onready var keyNum: Label = $HUD/HBoxContainer/KeyNum
@onready var coinNum: Label = $HUD/HBoxContainer/CoinNum
@onready var coinTotal = $HUD/HBoxContainer/CoinTotal
@onready var boltNum = $HUD/HBoxContainer/BoltNum

@onready var gameOver = $HUD/GameOver
@onready var finalTotal = $HUD/GameOver/Label/FinalTotal
@onready var button = $HUD/GameOver/Button

#@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var sprite: AnimatedSprite2D = $Sprite3D
#@onready var gunFlash: OmniLight3D = $GunFlash
@onready var lanternLight: OmniLight3D = $Lantern/OmniLight3D
@onready var timer: Timer = $Timer
#@onready var weapon = $Head/Camera3d/Weapon

#@export var _bullet_scene : PackedScene
##Mouse speed: lower = faster
#@export var mouseSpeed: float = 500
@export var jumpVelocity: float = 6

const bobFreq: float = 1.5
const bobAmp: float = 0.1
const baseFOV: float = 75
const fovChange: float = 3

var speed: float
var walkSpeed: float = 6
var runSpeed: float = 10
var burdenSpeed: float = 3
var hBob: float = 0
var mouse_relative_x: float = 0
var mouse_relative_y: float = 0
var moving: bool = false
var running: bool = false
var lanternOn: bool = false
var canMove: bool = true
var lerpSpeed: float = 10
var canShoot: bool = true
var canPlay: bool = false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	GameController.player = self
	gameOver.hide()
	#Captures mouse and stops gun from hitting yourself
#	gunRay.add_exception(self)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	sprite.animation_finished.connect(ShootAnimDone)
	
func _process(delta):
	lanternLight.light_energy = lerp(lanternLight.light_energy, float(GameController.oilLevel), delta * lerpSpeed)
	oilLevel.value = lanternLight.light_energy
	
	if GameController.oilLevel < 5:
		lanternLight.omni_range = lerp(lanternLight.omni_range, float(GameController.oilLevel) + 5, delta * lerpSpeed)
	
	if GameController.weight > 50:
		weightLevel.tint_progress = "ff0000"
	else:
		weightLevel.tint_progress = "ffffff"
	
	keyNum.text = str(GameController.keys.size() - 1)
	coinNum.text = str(GameController.coinsCollect)
	weightLevel.value = GameController.weight
	coinTotal.text = str(GameController.totalCoinsCollect)
	boltNum.text = str(GameController.boltsAmmo)
	
	if GameController.hasCrossbow:
		sprite.show()
	
func _input(event):
	if event is InputEventMouseMotion:
		rotation.y -= event.relative.x / GameController.mouseSpeed #left/right turn
		if GameController.invertLook:
			cam.rotation.x += event.relative.y / GameController.mouseSpeed
		else:
			cam.rotation.x -= event.relative.y / GameController.mouseSpeed
			
		cam.rotation.x = clamp(cam.rotation.x, deg_to_rad(-60), deg_to_rad(90) )
		mouse_relative_x = clamp(event.relative.x, -50, 50)
		mouse_relative_y = clamp(event.relative.y, -50, 10)

func _unhandled_input(event):
	if event.is_action_pressed("lantern") && GameController.hasLantern && !lanternOn && GameController.oilLevel > 0:
		lantern.show()
		lanternOn = true
		
		timer.start()
	elif event.is_action_pressed("lantern") && GameController.hasLantern && lanternOn:
		lantern.hide()
		lanternOn = false
		timer.stop()

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta * 2

	# Handle Jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = jumpVelocity
	# Handle Shooting this can crash if too many of the decal nodes are generated
	if Input.is_action_just_pressed("shoot") && GameController.boltsAmmo > 0:
		Shoot()
	
	if Input.is_action_pressed("run") && GameController.weight < 50:
		speed = runSpeed
	elif GameController.weight > 50:
		speed = burdenSpeed
	else:
		speed = walkSpeed
	# Get the input direction and handle the movement/deceleration.
	var input_dir = Input.get_vector("moveLeft", "moveRight", "moveUp", "moveDown")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	
	if is_on_floor():
		if direction:
			velocity.x = lerp(velocity.x, direction.x * speed, delta * 10)
			velocity.z = lerp(velocity.z, direction.z * speed, delta * 10)
		else:
			velocity.x = lerp(velocity.x, direction.x * speed, delta * 7.0)
			velocity.z = lerp(velocity.z, direction.z * speed, delta * 7.0)
	else:
		velocity.x = lerp(velocity.x, direction.x * speed, delta * 3.0)
		velocity.z = lerp(velocity.z, direction.z * speed, delta * 3.0)
		
	hBob += delta * velocity.length() * float(is_on_floor())
	cam.transform.origin = Headbob(hBob)
	
	if canMove:
		move_and_slide()

func Headbob(time):
	var pos = Vector3.ZERO
	pos.y = sin(time * bobFreq) * bobAmp
	pos.x = cos(time * bobFreq / 2) * bobAmp
	pos.z = -0.302
	
	var lowPos = bobAmp - 0.05
	
	if pos.y > -lowPos:
		canPlay = true
		
	if pos.y < -lowPos and canPlay:
		canPlay = false
		SoundFx.play("step")
	return pos

func Shoot():
	if !canShoot || GameController.boltsAmmo == 0:
		return
	canShoot = false

	sprite.play("shoot")
	
	GameController.boltsAmmo -= 1
	GameController.weight -= 1
	
	if gunRay.is_colliding() && gunRay.get_collider().has_method("Kill"):
		print("found")
		gunRay.get_collider().Kill()
	
func ShootAnimDone():
	canShoot = true
	
func Kill():
	SoundFx.play("playerHit")
	gameOver.show()
	finalTotal.text = str(GameController.totalCoinsCollect)
	get_tree().paused = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	print("pwnd")
	
#func Shoot():
#	if not gunRay.is_colliding():
#		return
#	var bulletInst = _bullet_scene.instantiate() as Node3D
#	bulletInst.set_as_top_level(true)
#	get_parent().add_child(bulletInst)
#	bulletInst.global_transform.origin = gunRay.get_collision_point() as Vector3
#	bulletInst.look_at((gunRay.get_collision_point()+gunRay.get_collision_normal()),Vector3.BACK)
#	print(gunRay.get_collision_point())
#	print(gunRay.get_collision_point()+gunRay.get_collision_normal())
#
#	gunFlash.light_energy = 5
#	await get_tree().create_timer(.05).timeout
#	gunFlash.light_energy = 0
#	GameController.boltsAmmo -= 1
	
func _on_timer_timeout():
	if GameController.oilLevel > 0:
		GameController.oilLevel -= 0.1
	else:
		lanternOn = false
		lantern.hide()

func _on_button_pressed():
	get_tree().change_scene_to_file("res://Scenes/Title.scn")
