extends Node2D

@onready var light = $PointLight2D

func _process(_delta):
	if DayNight.anim.current_animation_position > 300:
		light.enabled = false
	else:
		light.enabled = true
