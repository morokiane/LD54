extends Node

const gameScenes: Dictionary ={
	"town": "res://Levels/Town.scn"
}

var loadScreen = preload("res://Scenes/LoadingScreen.scn")

var player: CharacterBody3D
var invertLook: bool = false
var fullscreen: bool = false
var mouseReleased: bool = false
var mouseSpeed: float = 500
var inDungeon: bool = false

var hasLantern: bool = false
var hasCrossbow: bool = false
var oilLevel: float = 0.5
var coinsCollect: int = 0
var totalCoinsCollect: int = 0
var weight: float = 0
var boltsAmmo: int = 0
var playerHP: int = 1

var keys: Dictionary = {}

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene_to_file("res://Scenes/Title.scn")
		
	if event.is_action_pressed("mouse") && !mouseReleased:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		get_tree().paused = true
		mouseReleased = true
	elif event.is_action_pressed("mouse") && mouseReleased:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		get_tree().paused = false
		mouseReleased = false
	
#	if event.is_action_pressed("fullscreen") && !fullscreen:
#		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
#		fullscreen = true
#	elif event.is_action_pressed("fullscreen") && fullscreen:
#		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
#		fullscreen = false

func _ready():
	GameController.keys["None"] = true
	
func LoadScene(currentScene, nextScene):
	var loadScreenInstance = loadScreen.instantiate()
	get_tree().get_root().call_deferred("add_child", loadScreenInstance)
	
	var loadPath : String
	if gameScenes.has(nextScene):
		loadPath = gameScenes[nextScene]
	else:
		loadPath = nextScene
		
	var loaderNextScene
	if ResourceLoader.exists(loadPath):
		ResourceLoader.load_threaded_request(loadPath)
	
	if loaderNextScene == null:
		print("Error: Attempting to load non-existent file")
		return
	await loadScreenInstance.safeToLoad
	currentScene.queue_free()
		
	while true:
		var loadProgress = []
		var loadStatus = ResourceLoader.load_threaded_get_status(loadPath, loadProgress)
		
		match loadStatus:
			0:
				print("Cannot load")
				return
			1:
				loadScreenInstance.get_node("Control/ProgressBar").value = loadProgress[0]
			2:
				print("Cannot load")
				return
			3:
				var nextSceneInstance = ResourceLoader.load_threaded_get(loadPath).instantiate()
				get_tree().get_root().call_deferred("add_child", nextSceneInstance)
				
				loadScreenInstance.FadeOut()
				return
